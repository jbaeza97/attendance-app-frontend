import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManualListPage } from './manual-list.page';

describe('ManualListPage', () => {
  let component: ManualListPage;
  let fixture: ComponentFixture<ManualListPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManualListPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManualListPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
