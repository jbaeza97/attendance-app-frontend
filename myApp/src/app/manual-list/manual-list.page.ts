import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UadbackendService } from '../uadbackend.service';

@Component({
  selector: 'app-manual-list',
  templateUrl: './manual-list.page.html',
  styleUrls: ['./manual-list.page.scss'],
})
export class ManualListPage implements OnInit {

  overlayHiddenSuccess = true;
  subject;
  keys;
  isChecked = false;
  students = [];
  group;

  constructor(private router: Router, public service: UadbackendService) {}

  ngOnInit() {
    this.subject = localStorage.getItem('subject');
    this.service.getGroupStudents(localStorage.getItem('registry_id')).subscribe(data => {
      // console.log(data);
      this.group = data;
      this.keys = Object.keys(this.group);
      for (let i = 0; i < this.keys.length; i++) {
        this.students.push({
          registry_detail_id: this.group[i].id,
          first_name: this.group[i].student.first_name,
          last_name: this.group[i].student.last_name,
          missings: this.group[i].missings,
          total_missings_available: this.group[i].registry.session_hours_count
        });
      }
    });
  }

  increment(student_key) {
    if (this.students[student_key].missings < this.students[student_key].total_missings_available){
      this.students[student_key].missings += 1;
    }
  }

  decrement(student_key) {
    if (this.students[student_key].missings > 0) {
      this.students[student_key].missings -= 1;
    }
  }

  submitList() {
    for (let i = 0; i < this.students.length; i++) {
      this.service.updateStudents(this.students[i].registry_detail_id, this.students[i]).subscribe(data => {
        // console.log(data);
      });
    }
    this.overlayHiddenSuccess = false;
    setTimeout(this.opensnacksuccess.bind(this), 2000);
  }

  opensnacksuccess(){
    this.overlayHiddenSuccess = true;
    this.router.navigate(['teacher-classes']);
  }
}
