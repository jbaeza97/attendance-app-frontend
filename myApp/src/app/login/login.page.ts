import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UadbackendService } from '../uadbackend.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  data; //Object with user info
  user;
  error;
  NotAuthError:boolean = true;

  constructor(private router: Router, public service: UadbackendService) {
  }

  ngOnInit() {
    this.data = {
      username: "",
      password: ""
    }
  }

  authfailed():void {
    this.NotAuthError = true;
  }

  sendInfo(){
    // console.log(this.data)
    this.service.loginUser(this.data).subscribe(res => {
      this.user = res
      // console.log(this.user)
      if(this.user['role'] == 'teacher'){
        localStorage.setItem('teacher_id', this.user['id']);
        localStorage.setItem('first_name', this.user['first_name']);
        this.router.navigate(['teacher-classes']);
        // console.log("Teacher logged in")
      }else if(this.user['role'] == 'student'){
        localStorage.setItem('student_id', this.user['id']);
        localStorage.setItem('first_name', this.user['first_name']);
        this.router.navigate(['student-classes']);
        // console.log("Student logged in")
      }
    }, err => {
      this.error = err
      if(this.error.status == 401 || this.error.status == 500 ){
        this.NotAuthError = false
        setTimeout(this.authfailed.bind(this), 1500);
      }
    });
  }
}
