import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-url',
  templateUrl: './url.page.html',
  styleUrls: ['./url.page.scss'],
})
export class UrlPage implements OnInit {

  url;

  constructor(private router: Router) { }

  ngOnInit() {
    
  }

  send(url){
    localStorage.setItem('url', url);
    // console.log(url);
    this.router.navigate(['login']);
  }
}
