import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { CodeGeneratePage } from './code-generate.page';

const routes: Routes = [
  {
    path: '',
    component: CodeGeneratePage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [CodeGeneratePage]
})
export class CodeGeneratePageModule {}
