import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { UadbackendService } from '../uadbackend.service';

@Component({
  selector: 'app-code-generate',
  templateUrl: './code-generate.page.html',
  styleUrls: ['./code-generate.page.scss'],
})
export class CodeGeneratePage implements OnInit, OnDestroy {

  overlayHiddenSuccess = true;
  overlayHiddenFailed = true;
  overlayHiddenConfirm = true;
  code; subject; group; keys; missings; students = []; interval;

  constructor(private router: Router, public service: UadbackendService) {}

  ngOnInit() {
    this.service.getCode().subscribe(data => {
      this.code = data;
    });
    this.subject = localStorage.getItem('subject');
    this.service.getGroupStudents(localStorage.getItem('registry_id')).subscribe(data => {
      this.group = data;
      // console.log(this.group)
      this.keys = Object.keys(this.group);
      for(let i = 0; i < this.keys.length; i++){
        this.students.push({
          registry_detail_id: this.group[i].id,
          first_name: this.group[i].student.first_name,
          last_name: this.group[i].student.last_name,
          missings: this.group[i].registry.session_hours_count,
        });
      }
      for(let i = 0; i < this.students.length; i++) {
        this.service.updateStudents(this.students[i].registry_detail_id, this.students[i]).subscribe(data => {
          // console.log(data)
        });
      }
    });
    this.interval = setInterval(this.generateCode.bind(this), 15000);
  }

  generateCode() {
    this.service.getCode().subscribe(data => this.code = data);
    this.service.getGroupStudents(localStorage.getItem('registry_id')).subscribe(data => {
      this.group = data;
      this.keys = Object.keys(this.group);
      this.students = [];
      for(let i = 0; i < this.keys.length; i++) {
        this.students.push({
          registry_detail_id: this.group[i].id,
          first_name: this.group[i].student.first_name,
          last_name: this.group[i].student.last_name,
          missings: this.group[i].missings,
        });
      }
    });
  }

  ngOnDestroy() {
    clearInterval(this.interval);
  }

  goTeacherSchedule() {
    this.overlayHiddenConfirm = true;
    this.overlayHiddenSuccess = false;
    setTimeout(this.opensnacksuccess.bind(this), 1000);
  }

  close() {
    this.overlayHiddenConfirm = true;
  }

  opensnacksuccess(): void {
    this.overlayHiddenSuccess = true;
    this.router.navigate(['teacher-classes']);
  }

  submitList() {
    clearInterval(this.interval);
    this.overlayHiddenConfirm = false;
  }
}
