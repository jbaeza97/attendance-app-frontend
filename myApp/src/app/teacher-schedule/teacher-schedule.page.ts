import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UadbackendService } from '../uadbackend.service';

@Component({
  selector: 'app-teacher-schedule',
  templateUrl: './teacher-schedule.page.html',
  styleUrls: ['./teacher-schedule.page.scss'],
})
export class TeacherSchedulePage implements OnInit {

  subject; keys; lists; registry;
  constructor(private router: Router, public service: UadbackendService) { }

  ngOnInit() {
    // this.subject = localStorage.getItem('subject');
    // this.service.getRegistries(this.subject).subscribe(data => {
    //   this.lists = data;      
    //   this.keys = Object.keys(this.lists);
    //   console.log(this.lists , this.keys);
    // });
  }

  ionRefresh(event) {
  //   console.log('Pull Event Triggered!');
  //   this.subject = localStorage.getItem('subject');
  //   this.service.getRegistries(this.subject).subscribe(data => {
  //     this.lists = data;
  //     this.keys = Object.keys(this.lists);
  //   });
  //   setTimeout(() => {
  //     console.log('Async operation has ended');

  //     //complete()  signify that the refreshing has completed and to close the refresher
  //     event.target.complete();
  //   }, 1000);
  // }
  // ionPull(event){
  //   //Emitted while the user is pulling down the content and exposing the refresher.
  //   console.log('ionPull Event Triggered!');
  // }
  // ionStart(event){
  //   //Emitted when the user begins to start pulling down.
  //   console.log('ionStart Event Triggered!');
  // }

  // createRegistry(){
  //   this.service.createRegistry(
  //     localStorage.getItem('subject'),
  // 		localStorage.getItem('group'),
  // 		localStorage.getItem('name'),
  // 		localStorage.getItem('start_time'),
  //     localStorage.getItem('end_time')).subscribe(data => {
  //       console.log(data);
  //     });
  //     this.subject = localStorage.getItem('subject');
  //     let temp = this
  //     setTimeout(function(){
  //       temp.service.getRegistries(temp.subject).subscribe(data => {
  //         temp.lists = data;
  //         temp.keys = Object.keys(temp.lists);
  //       });
  //     }, 1000);
  // }

  // generateCode(registry_id){
  //   this.router.navigate(['code-generate']);
  //   localStorage.setItem('registry_id', registry_id);
  // }

  // generateManualList(registry_id){
  //   this.router.navigate(['manual-list']);
  //   localStorage.setItem('registry_id', registry_id);
  }
}

