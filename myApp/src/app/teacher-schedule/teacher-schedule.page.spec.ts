import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TeacherSchedulePage } from './teacher-schedule.page';

describe('TeacherSchedulePage', () => {
  let component: TeacherSchedulePage;
  let fixture: ComponentFixture<TeacherSchedulePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TeacherSchedulePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TeacherSchedulePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
