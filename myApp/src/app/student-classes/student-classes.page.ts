import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UadbackendService } from '../uadbackend.service';

@Component({
  selector: 'app-student-classes',
  templateUrl: './student-classes.page.html',
  styleUrls: ['./student-classes.page.scss'],
})
export class StudentClassesPage implements OnInit {

    
  keys; subject; hour; cards; user; firstname; no_classes

  constructor(private router: Router, public service: UadbackendService) {}

  ngOnInit() {
    this.user = localStorage.getItem('first_name');
    this.service.getStudentCard(localStorage.getItem('student_id')).subscribe(data => {
      this.cards = data;
      // console.log(this.cards.length)
      if(this.cards.length == 0){
        this.no_classes = true
      }
      this.keys = Object.keys(this.cards);
    });
  }

  redeemCode(registry_id){
    localStorage.setItem('registry_id', registry_id);
    this.router.navigate(['code-register']);
  }
}
