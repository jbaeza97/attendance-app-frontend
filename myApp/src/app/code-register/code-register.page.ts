import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UadbackendService } from '../uadbackend.service';

@Component({
  selector: 'app-code-register',
  templateUrl: './code-register.page.html',
  styleUrls: ['./code-register.page.scss'],
})
export class CodeRegisterPage implements OnInit {

  overlayHiddenSuccess: boolean = true;
  overlayHiddenFailed: boolean = true;
  validation; lists; keys; json = []; id;

  constructor(private router: Router, public service: UadbackendService) {}

  ngOnInit() {}

  sendCode(code){
    this.service.sendCode(code, localStorage.getItem('student_id'), localStorage.getItem('registry_id')).subscribe(data => {
      this.validation = data;
      // console.log(this.validation);
      if (this.validation['msg'] == 'Success') {
        // UPDATE STUDENT STATUS
        this.overlayHiddenSuccess = false;
        setTimeout(this.opensnacksuccess.bind(this), 1500);
      } else if (data['msg'] == 'Wrong code') {
        this.overlayHiddenFailed = false;
        setTimeout(this.opensnackfailed.bind(this), 1500);
      }
    });
  }
  opensnackfailed(): void {
    this.overlayHiddenFailed = true;
  }

  opensnacksuccess(): void {
    this.overlayHiddenSuccess = true;
    this.router.navigate(['student-classes']);
  }
}
