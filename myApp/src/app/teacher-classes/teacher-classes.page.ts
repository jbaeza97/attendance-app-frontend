import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UadbackendService } from '../uadbackend.service';

@Component({
  selector: 'app-teacher-classes',
  templateUrl: './teacher-classes.page.html',
  styleUrls: ['./teacher-classes.page.scss'],
})
export class TeacherClassesPage implements OnInit {

  keys; user; cards; data; no_classes; validation = {};

  constructor(private router: Router, public service: UadbackendService) { }

  ngOnInit() {
    this.user = localStorage.getItem('first_name');
    this.service.getTeacherClasses(localStorage.getItem('teacher_id')).subscribe(data => {
      this.cards = data;
      if (this.cards.length == 0) {
        this.no_classes = true;
      }
      this.keys = Object.keys(this.cards);
      // console.log(this.cards);
    });
  }

  generateManualList(registry_id, subject) {
    this.router.navigate(['manual-list']);
    localStorage.setItem('registry_id', registry_id);
    localStorage.setItem('subject', subject);
    this.validation['registry_id'] = registry_id;
    this.validation['validation'] = 1;
    // console.log('sending validation')
    // console.log('sent:', this.validation)
    this.service.updateAssistanceTaken(registry_id, this.validation).subscribe(data => {
      // console.log(data);
    });
  }

  generateCode(registry_id, subject) {
    this.router.navigate(['code-generate']);
    localStorage.setItem('registry_id', registry_id);
    localStorage.setItem('subject', subject);
    this.validation['registry_id'] = registry_id;
    this.validation['validation'] = 1;
    // console.log('sending validation')
    // console.log('sent:', this.validation)
    this.service.updateAssistanceTaken(registry_id, this.validation).subscribe(data => {
      // console.log(data);
    });
  }
}
