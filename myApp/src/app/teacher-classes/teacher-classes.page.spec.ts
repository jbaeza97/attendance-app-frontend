import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TeacherClassesPage } from './teacher-classes.page';

describe('TeacherClassesPage', () => {
  let component: TeacherClassesPage;
  let fixture: ComponentFixture<TeacherClassesPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TeacherClassesPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TeacherClassesPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
