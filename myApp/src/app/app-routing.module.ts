import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  { path: '', loadChildren: './login/login.module#LoginPageModule' },
  { path: 'teacher-classes', loadChildren: './teacher-classes/teacher-classes.module#TeacherClassesPageModule' },
  { path: 'teacher-schedule', loadChildren: './teacher-schedule/teacher-schedule.module#TeacherSchedulePageModule' },
  { path: 'code-generate', loadChildren: './code-generate/code-generate.module#CodeGeneratePageModule' },
  { path: 'manual-list', loadChildren: './manual-list/manual-list.module#ManualListPageModule' },
  { path: 'student-classes', loadChildren: './student-classes/student-classes.module#StudentClassesPageModule' },
  { path: 'code-register', loadChildren: './code-register/code-register.module#CodeRegisterPageModule' },
  { path: 'url', loadChildren: './url/url.module#UrlPageModule' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
