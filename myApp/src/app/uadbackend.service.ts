import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import {
  StudentInterface,
  TeacherInterface,
  GroupInterface,
  RegistriesInterface,
  UserInterface,
} from './backend';
// import 'rxjs/add/operator/catch'
// import 'rxjs/add/observable/throw'

@Injectable({
  providedIn: 'root'
})
export class UadbackendService {

  public url = 'https://joel.eapl.mx/';
  // public url = 'http://127.0.0.1:8000/';

  constructor(private http: HttpClient) { }

  /**
   * Login as teacher or student
   * @param email
   * @param password
   */
  loginUser(data): Observable<UserInterface[]> {
    let url = this.url + 'login/';
    return this.http.post<UserInterface[]>(url, data);
  }

  /**
   * Get code from backend for teacher to show
   */
  getCode(): Observable<TeacherInterface[]> {
    const headers = new HttpHeaders().set('Accept', 'application/json');
    let url = this.url + 'code/';
    return this.http.get<StudentInterface[]>(url, { headers });
  }

  /**
   * Send code for verification
   * @param data 
   */
  sendCode(code, student_id, registry): Observable<StudentInterface[]> {
    const headers = new HttpHeaders().set('Accept', 'application/json');
    let url = this.url + "code/";
    return this.http.post<StudentInterface[]>(url, { code: code, student_id: student_id, registry_id: registry }, { headers });
  }

  /**
   * Get the teacher classes by the id
   * @param id 
   */
  getTeacherClasses(id): Observable<RegistriesInterface> {
    const headers = new HttpHeaders().set('Accept', 'application/json');
    let url = this.url + 'registry-teachers/' + id;
    return this.http.get<RegistriesInterface[]>(url, { headers });
  }

  /**
   * Get student classes
   * @param email 
   */
  getStudentCard(student_id): Observable<StudentInterface[]> {
    const headers = new HttpHeaders().set('Accept', 'application/json');
    let url = this.url + 'registry-students/' + student_id;
    return this.http.get<StudentInterface[]>(url, { headers });
  }

  /**
   * Get all students within a group passing an id
   * @param id 
   */
  getGroupStudents(registry_id): Observable<GroupInterface[]> {
    const headers = new HttpHeaders().set('Accept', 'application/json');
    let url = this.url + 'registry-details/' + registry_id;
    return this.http.get<GroupInterface[]>(url, { headers });
  }

  updateStudents(registry_id, student): Observable<StudentInterface[]> {
    const headers = new HttpHeaders().set('Accept', 'application/json');
    let url = this.url + 'registry-details/' + registry_id;
    return this.http.put<StudentInterface[]>(url, student, { headers });
  }

  updateAssistanceTaken(registry_id, validation): Observable<StudentInterface[]> {
    const headers = new HttpHeaders().set('Accept', 'application/json');
    let url = this.url + 'registry-teachers/' + registry_id;
    return this.http.put<StudentInterface[]>(url, validation, { headers });
  }
}
